package com.shamoon.earthquake.service

import android.content.Context
import com.shamoon.earthquake.R
import com.shamoon.earthquake.service.model.EarthquakeResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface EarthquakeAPI {
    companion object {
        fun create(api: String): EarthquakeAPI {
            val retrofit = Retrofit.Builder()
                    .baseUrl(api)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(EarthquakeAPI::class.java)
        }
    }
    @GET("earthquakesJSON")
    fun getEarthquakeData(@Query("formatted") formatted: Boolean = true,
                              @Query("north") north: Float = 44.1f,
                              @Query("south") south: Float = -9.9f,
                              @Query("east") east: Float = -22.4f,
                              @Query("west") west: Float = 55.2f,
                              @Query("username") username: String = "mkoppelman"
    ): Call<EarthquakeResponse>
}