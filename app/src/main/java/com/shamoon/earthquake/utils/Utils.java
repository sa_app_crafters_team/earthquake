package com.shamoon.earthquake.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.shamoon.earthquake.model.room.entitie.Earthquake;

import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static List<Earthquake> convertAPITypeToDBType(List<com.shamoon.earthquake.service.model.Earthquake> apiList){
        List<Earthquake> list = new ArrayList<>();
        for (com.shamoon.earthquake.service.model.Earthquake item: apiList){
            list.add(new Earthquake(
                    item.getDatetime(),
                    item.getDepth(),
                    item.getLng(),
                    item.getSrc(),
                    item.getEqid(),
                    item.getMagnitude(),
                    item.getLat()
            ));
        }
        return list;
    }
}
