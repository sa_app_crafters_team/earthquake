package com.shamoon.earthquake.view.fragments.quake

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.shamoon.earthquake.R
import com.shamoon.earthquake.databinding.QuakeBinding


class Quake : Fragment(), OnMapReadyCallback {
    private lateinit var binding: QuakeBinding
    private lateinit var mMap: GoogleMap

    companion object {
        fun newInstance() = Quake()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.quake, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.map.onCreate(savedInstanceState);
        try {
            MapsInitializer.initialize(activity?.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        binding.map.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isMyLocationButtonEnabled = false
        val earthquakeLocation = LatLng(
                requireArguments().getString("lat")!!.toDouble(),
                requireArguments().getString("lng")!!.toDouble())

        //showing custom icon for earthquake and showing magnitude as title of marker because it's relevant information for earthquake marker
        mMap.addMarker(MarkerOptions().position(earthquakeLocation).title(
                getString(R.string.magnitude)
                        + ": " +
                        requireArguments().getString("magnitude"))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_earthquake)))

        mMap.moveCamera(CameraUpdateFactory.newLatLng(earthquakeLocation))
    }

    override fun onResume() {
        super.onResume()
        if (binding != null)
            binding.map.onResume()
    }

    override fun onPause() {
        super.onPause()
        binding.map.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.map.onDestroy()
    }

    override fun onStop() {
        super.onStop()
        binding.map.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        binding.map.onLowMemory()
    }


}