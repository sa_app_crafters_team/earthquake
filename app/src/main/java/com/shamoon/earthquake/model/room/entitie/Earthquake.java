package com.shamoon.earthquake.model.room.entitie;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "earthquake")
public class Earthquake {
    @NonNull
    @PrimaryKey
    private String eqid;

    @ColumnInfo(name = "datetime")
    private String datetime;
    @ColumnInfo(name = "depth")
    private Double depth;
    @ColumnInfo(name = "lng")
    private Double lng;
    @ColumnInfo(name = "src")
    private String src;
    @ColumnInfo(name = "magnitude")
    private Double magnitude;
    @ColumnInfo(name = "lat")
    private Double lat;

    public Earthquake() {
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void setDepth(Double depth) {
        this.depth = depth;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public void setEqid(String eqid) {
        this.eqid = eqid;
    }

    public void setMagnitude(Double magnitude) {
        this.magnitude = magnitude;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }


    public Earthquake(String datetime, Double depth, Double lng, String src, String eqid, Double magnitude, Double lat) {
        this.datetime = datetime;
        this.depth = depth;
        this.lng = lng;
        this.src = src;
        this.eqid = eqid;
        this.magnitude = magnitude;
        this.lat = lat;
    }

    public String getSrc() {
        return src;
    }

    public Double getMagnitude() {
        return magnitude;
    }

    public Double getLng() {
        return lng;
    }

    public Double getLat() {
        return lat;
    }

    public Double getDepth() {
        return depth;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getEqid() {
        return eqid;
    }
}
