package com.shamoon.earthquake.viewmodel;

import android.content.Context;
import android.view.View;

import androidx.databinding.ObservableInt;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.shamoon.earthquake.R;
import com.shamoon.earthquake.model.room.entitie.Earthquake;
import com.shamoon.earthquake.service.repositories.EarthquakeRepository;
import com.shamoon.earthquake.view.adapters.RecyclerViewAdapter;

import java.util.List;

public class EarthquakeListViewModel extends ViewModel {
    EarthquakeRepository repository;
    private Context context;
    private RecyclerViewAdapter adapter;
    public MutableLiveData<Earthquake> selected;
    public ObservableInt loading;
    public ObservableInt showEmpty;

    public void init() {
        repository = new EarthquakeRepository();
        selected = new MutableLiveData<>();
        adapter = new RecyclerViewAdapter(R.layout.list_item, this);
        loading = new ObservableInt(View.GONE);
        showEmpty = new ObservableInt(View.GONE);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void onItemClick(Integer index) {
        Earthquake db = getEarthquakeAt(index);
        selected.setValue(db);
    }

    public MutableLiveData<Earthquake> getSelected() {
        return selected;
    }

    public LiveData<List<Earthquake>> fetchListFromDB(){
        return repository.getEarthquakeDataFromDB(context);
    }

    public void fetchListFromAPI() {
        repository.getEarthquakeDataFromAPI(context);
    }

    public RecyclerViewAdapter getAdapter() {
        return adapter;
    }

    public void setEarthquakesInAdapter(List<Earthquake> earthquakeList) {
        this.adapter.setEarthquakeList(earthquakeList);
        this.adapter.notifyDataSetChanged();
    }

    public Earthquake getEarthquakeAt(Integer index) {
        if (repository.getEarthquake().getValue() != null &&
                index != null &&
                repository.getEarthquake().getValue().size() > index) {
            return repository.getEarthquake().getValue().get(index);
        }
        return null;
    }

}