package com.shamoon.earthquake

import android.app.Instrumentation
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class ResourceInstrumentedTest {
    @Test
    fun canReadAPI(){
        var apiURL: String = InstrumentationRegistry.getInstrumentation().targetContext.resources.getString(R.string.api)
        Assert.assertNotNull(apiURL)
        Assert.assertEquals("it is correct URL", "http://api.geonames.org/", apiURL)
    }
    fun getAPI(): String{
        canReadAPI()
        return InstrumentationRegistry.getInstrumentation().targetContext.resources.getString(R.string.api)
    }

    @Test
    fun canReadNoInternetConnectionStr(){
        var noInternetStr: String = InstrumentationRegistry.getInstrumentation().targetContext.resources.getString(R.string.connection_issue)
        Assert.assertNotNull(noInternetStr)
        Assert.assertEquals("it is correct message", "Please check your internet connection.", noInternetStr)
    }

    fun getNoInternetConnectionStr(): String{
        canReadNoInternetConnectionStr()
        return InstrumentationRegistry.getInstrumentation().targetContext.resources.getString(R.string.connection_issue)
    }
}