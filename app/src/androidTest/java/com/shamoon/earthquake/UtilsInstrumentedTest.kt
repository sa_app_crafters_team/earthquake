package com.shamoon.earthquake

import android.app.Instrumentation
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.shamoon.earthquake.utils.Utils
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UtilsInstrumentedTest {
    @Test
    fun isInternetConnected(){
        var isConnected: Boolean = Utils.isNetworkAvailable(InstrumentationRegistry.getInstrumentation().targetContext)
        Assert.assertNotNull(isConnected)
    }

    fun getIsInternetConnected(): Boolean{
        isInternetConnected()
        return  Utils.isNetworkAvailable(InstrumentationRegistry.getInstrumentation().targetContext)
    }
}