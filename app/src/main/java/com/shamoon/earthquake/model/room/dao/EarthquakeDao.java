package com.shamoon.earthquake.model.room.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.shamoon.earthquake.model.room.entitie.Earthquake;

import java.util.List;

@Dao
public abstract class EarthquakeDao {
    @Query("Select * from Earthquake")
    public abstract LiveData<List<Earthquake>> getEarthquakeList();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract void insertEarthquake(Earthquake earthquake);

    @Query("SELECT EXISTS(SELECT * FROM Earthquake WHERE eqid = :eqid)")
    public abstract Boolean isRowIsExist(String eqid);

    @Transaction
    public void deleteAndInsertEarthquake(List<Earthquake> earthquakeList) {
        //in given api, we have hardcoded username, but if username will be changed then I will write different logic i.e: if username is changed then delete all records and insert all again but in this case
        //there is no point to put same data again and again in database so I managed this by a simple if condition
        for (Earthquake earthquake : earthquakeList) {
            if (!isRowIsExist(earthquake.getEqid()))
                insertEarthquake(earthquake);
        }

    }
}
