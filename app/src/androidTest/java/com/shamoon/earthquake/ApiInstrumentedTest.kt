package com.shamoon.earthquake

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shamoon.earthquake.service.EarthquakeAPI
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ApiInstrumentedTest {
    @Test
    fun canCallEarthquakeAPI() {
        //call the api
        if (UtilsInstrumentedTest().getIsInternetConnected()){
            val api = EarthquakeAPI.create(ResourceInstrumentedTest().getAPI())
            val response = api.getEarthquakeData().execute()
            //Assert.assertEquals("API is called", true, api.getEarthquakeData().isExecuted)
            Assert.assertEquals("API calling successful", 200, response.code())
            Assert.assertEquals("API got JSON for us", true, response.isSuccessful)
        }else{
            Assert.assertFalse(ResourceInstrumentedTest().getNoInternetConnectionStr(), UtilsInstrumentedTest().getIsInternetConnected())
        }
    }
}