package com.shamoon.earthquake.model.room;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.shamoon.earthquake.model.room.dao.EarthquakeDao;
import com.shamoon.earthquake.model.room.entitie.Earthquake;

@androidx.room.Database(entities = Earthquake.class, exportSchema = true, version = 1)
public abstract class Database extends RoomDatabase {
    private static final String DB_NAME = "earthquake_db";
    private static Database instance;

    public static synchronized Database getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(
                    context.getApplicationContext(),
                    Database.class,
                    DB_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract EarthquakeDao earthquakeDao();
}
