package com.shamoon.earthquake.view.fragments.quakelist;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.shamoon.earthquake.R;
import com.shamoon.earthquake.databinding.EarthquakeListBinding;
import com.shamoon.earthquake.model.room.Database;
import com.shamoon.earthquake.model.room.entitie.Earthquake;
import com.shamoon.earthquake.viewmodel.EarthquakeListViewModel;

import java.util.List;

public class EarthquakeList extends Fragment {

    private EarthquakeListViewModel viewModel;
    private EarthquakeListBinding binding;

    public static EarthquakeList newInstance() {
        return new EarthquakeList();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.earthquake_list, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupBindings(savedInstanceState);
    }

    private void setupBindings(Bundle savedInstanceState) {
        viewModel = new ViewModelProvider(this).get(EarthquakeListViewModel.class);
        if (savedInstanceState == null) {
            viewModel.init();
        }
        viewModel.setContext(getContext());
        binding.setModel(viewModel);
        setupList();
    }

    private void setupList() {
        viewModel.loading.set(View.VISIBLE);
        viewModel.fetchListFromDB().observe(getViewLifecycleOwner(), earthquakeList -> {
            viewModel.loading.set(View.GONE);
            if (earthquakeList.size() == 0) {
                viewModel.showEmpty.set(View.VISIBLE);
            } else {
                viewModel.showEmpty.set(View.GONE);
                viewModel.setEarthquakesInAdapter(earthquakeList);
            }
        });
        setupListClick();

        viewModel.fetchListFromAPI();
    }

    private void setupListClick() {
        viewModel.getSelected().observe(getViewLifecycleOwner(), earthquake -> {
            if (earthquake != null) {
                EarthquakeListDirections.ActionEarthquakeListToQuake action = EarthquakeListDirections.actionEarthquakeListToQuake();
                action.setLat(earthquake.getLat().toString());
                action.setLng(earthquake.getLng().toString());
                action.setMagnitude(earthquake.getMagnitude().toString());

                NavHostFragment.findNavController(this).navigate(action);
            }
        });
    }

}