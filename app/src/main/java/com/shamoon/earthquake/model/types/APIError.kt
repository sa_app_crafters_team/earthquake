package com.shamoon.earthquake.model.types

class APIError {
    //we have to use ? operator while assigning value to it because they are nullable
    var message: String? = null
    var exists: Boolean? = null
}