package com.shamoon.earthquake.view.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.shamoon.earthquake.BR;
import com.shamoon.earthquake.model.room.entitie.Earthquake;
import com.shamoon.earthquake.viewmodel.EarthquakeListViewModel;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewViewHolder> {
    private int layoutId;
    private List<Earthquake> earthquakeList;
    private EarthquakeListViewModel viewModel;

    public RecyclerViewAdapter(@LayoutRes int layoutId, EarthquakeListViewModel viewModel) {
        this.layoutId = layoutId;
        this.viewModel = viewModel;
    }

    private int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @NonNull
    @Override
    public RecyclerViewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false);

        return new RecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewViewHolder holder, int position) {
        holder.bind(viewModel, position);
    }

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);
    }

    public void setEarthquakeList(List<Earthquake> earthquakeList) {
        this.earthquakeList = earthquakeList;
    }

    @Override
    public int getItemCount() {
        return earthquakeList == null ? 0 : earthquakeList.size();
    }

    class RecyclerViewViewHolder extends RecyclerView.ViewHolder {
        final ViewDataBinding binding;

        public RecyclerViewViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(EarthquakeListViewModel viewModel, Integer position) {
            viewModel.getEarthquakeAt(position);
            binding.setVariable(BR.viewModel, viewModel);
            binding.setVariable(BR.position, position);
            binding.executePendingBindings();
        }
    }
}
