# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This application reads JSON data from an earthquake API and populate a list within main View(Fragment) and tapping an earthquake in list open a secondary screen with location depicted on a map
* Version: 1.0
* Repository: https://shamoon192@bitbucket.org/shamoon192/earthquake.git
* Author: Shamoon Ahmed

### Project setup ###

* First you need to clone repository in your directory using following command
    git clone https://shamoon192@bitbucket.org/shamoon192/earthquake.git
* You need to install Android studio and Android SDK installed in your development machine to run this project, kindly download android studio from following link:
    https://developer.android.com/studio
* Project structure: Although Gradle offers a large degree of flexibility in your project structure, unless you have a compelling reason to do otherwise, you should accept its default structure as this simplify your build scripts.
* Configuration
    - For General structure follow Google's guide on Gradle for Android at https://developer.android.com/studio/build/index.html
    - Android studio will install all required tools to run this project from SDK and platform tools
    - before you do anything in project, make sure that you have created your separate branch from dev branch because only author have permission to change and merge in master and dev
    - if you want to run app on actual device then make sure that you have enabled USB debuging in your android settings, once connected then android studio will show your device name selected in connected devices dropdown and then you can run app
    - if you want to run app on emulator, first create one from AVD, run the AVD and then run application to it.
* Following dependencies are used in this project:
    - kotlin
    - retrofit2
    - room
    - multidex
    - lifecycle-viewmodel
    - play-services-maps
    - androidx.navigation
* Database is written in room which is in room package inside model
* I have written some instrumented tests and to run them, you need to run the test method with @Test annotation and make sure that your emulator or device is connected to android studio
* Deployment instructions

### Who do I talk to? ###

* Contact Author go to link: https://shamoonahmed1992.wixsite.com/shamooncv/
* send your message at https://shamoonahmed1992.wixsite.com/shamooncv/#comp-jtk9yd9216