package com.shamoon.earthquake.service.repositories

import android.content.Context
import android.util.Log
import androidx.databinding.BaseObservable
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.shamoon.earthquake.R
import com.shamoon.earthquake.model.room.Database
import com.shamoon.earthquake.model.room.entitie.Earthquake
import com.shamoon.earthquake.model.types.APIError
import com.shamoon.earthquake.service.EarthquakeAPI
import com.shamoon.earthquake.service.model.EarthquakeResponse
import com.shamoon.earthquake.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.logging.Level
import java.util.logging.Logger

class EarthquakeRepository : BaseObservable() {
    private val messageToShow: MutableLiveData<APIError> by lazy {
        MutableLiveData<APIError>()
    }

    private lateinit var earthquakeList: LiveData<List<Earthquake>>

    fun getEarthquakeDataFromDB(context: Context): LiveData<List<Earthquake>> {
        earthquakeList = Database.getInstance(context).earthquakeDao().earthquakeList

        return earthquakeList
    }

    fun getEarthquakeDataFromAPI(context: Context) {
        val database: Database = Database.getInstance(context)
        if (Utils.isNetworkAvailable(context)) {
            val callback: Callback<EarthquakeResponse> =
                    object : Callback<EarthquakeResponse> {
                        override fun onFailure(call: Call<EarthquakeResponse>, t: Throwable) {
                            Logger.getGlobal().log(Level.WARNING, t.message)
                            messageToShow.value?.message = t.message.toString()
                            messageToShow.value?.exists = true
                        }

                        override fun onResponse(call: Call<EarthquakeResponse>, response: Response<EarthquakeResponse>) {
                            if (response.isSuccessful) {
                                messageToShow.value?.exists = false
                                //first we need to delete everything in database to make sure that we are downloading old data
                                //we need to insert all data into database after deleting old data
                                database.earthquakeDao().deleteAndInsertEarthquake(Utils.convertAPITypeToDBType(response.body()?.earthquakes))
                            } else {
                                messageToShow.value?.exists = true
                                messageToShow.value?.message = response.message()
                            }


                        }
                    }
            EarthquakeAPI.create(context.getString(R.string.api)).getEarthquakeData().enqueue(callback)
        } else {
            //internet is not available, here we need to set message for user to check their connection
            messageToShow.value?.message = context.getString(R.string.connection_issue)
            messageToShow.value?.exists = true
        }
    }

    fun getEarthquake(): LiveData<List<Earthquake>> {
        return earthquakeList
    }
}